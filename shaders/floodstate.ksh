
   floodstate   	   MatrixPVW                                                                                SAMPLER    +         LIGHTMAP_WORLD_EXTENTS                                tilestate.vs�  #define VERTEX_SHADER

uniform mat4 MatrixPVW;

attribute vec3 POSITION;
attribute vec3 TEXCOORD0_LIFE;
attribute vec4 DIFFUSE;

varying vec3 PS_POS;
varying vec2 PS_TEXCOORD;
varying vec4 PS_COLOUR;

const float ATLAS_X_TEX_COUNT = 8.0;
const float ATLAS_Y_TEX_COUNT = 6.0;

vec2 get_texture_index(float lifetime)
{
	float index = -floor(lifetime) - 1.0;
	return vec2(mod(index, ATLAS_X_TEX_COUNT), floor(index / ATLAS_X_TEX_COUNT));
}
const float TEXTURE_WIDTH = 1024.0;
const float TEXTURE_HEIGHT = 1536.0;

#if defined (VERTEX_SHADER)
	const float MASK_X_OFFSET = 4.0;
	const float MASK_Y_OFFSET = 772.0;
	const float F_MASK_SIZE = 120.0;
	const float F_MASK_INDEX_SIZE = F_MASK_SIZE+8.0;

	const vec2 MASK_START = vec2(MASK_X_OFFSET/TEXTURE_WIDTH, MASK_Y_OFFSET/TEXTURE_HEIGHT);
	const vec2 MASK_SIZE = vec2(F_MASK_SIZE/TEXTURE_WIDTH, F_MASK_SIZE/TEXTURE_HEIGHT);
	const vec2 MASK_INDEX_SIZE = vec2(F_MASK_INDEX_SIZE/TEXTURE_WIDTH, F_MASK_INDEX_SIZE/TEXTURE_HEIGHT);
#elif defined (PIXEL_SHADER)
	const float NOISE_REPEAT_SIZE = 1.0/29.0;

	const float NOISE_X_OFFSET = 16.0;
	const float NOISE_Y_OFFSET = 16.0;
	const float F_NOISE_SIZE = 512.0;

	const vec2 NOISE_START = vec2(NOISE_X_OFFSET/TEXTURE_WIDTH, NOISE_Y_OFFSET/TEXTURE_HEIGHT);
	const vec2 NOISE_SIZE = vec2(F_NOISE_SIZE/TEXTURE_WIDTH, F_NOISE_SIZE/TEXTURE_HEIGHT);
#endif

void main() {
	vec2 texture_index = get_texture_index(TEXCOORD0_LIFE.z);
	vec3 pos = floor(POSITION + 0.5);

	gl_Position = MatrixPVW * vec4(pos, 1.0);

	PS_POS.xyz = pos;
	PS_TEXCOORD = MASK_START + (texture_index * MASK_INDEX_SIZE) + (TEXCOORD0_LIFE.xy * MASK_SIZE);
	PS_COLOUR = DIFFUSE;
	PS_COLOUR.rgb *= PS_COLOUR.a;
}

    floodstate.ps�  #define PIXEL_SHADER
#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D SAMPLER[4];

varying vec2 PS_TEXCOORD;
varying vec4 PS_COLOUR;

// Already defined by lighting.glh
// varying vec3 PS_POS

const float TEXTURE_WIDTH = 1024.0;
const float TEXTURE_HEIGHT = 1536.0;

#if defined (VERTEX_SHADER)
	const float MASK_X_OFFSET = 4.0;
	const float MASK_Y_OFFSET = 772.0;
	const float F_MASK_SIZE = 120.0;
	const float F_MASK_INDEX_SIZE = F_MASK_SIZE+8.0;

	const vec2 MASK_START = vec2(MASK_X_OFFSET/TEXTURE_WIDTH, MASK_Y_OFFSET/TEXTURE_HEIGHT);
	const vec2 MASK_SIZE = vec2(F_MASK_SIZE/TEXTURE_WIDTH, F_MASK_SIZE/TEXTURE_HEIGHT);
	const vec2 MASK_INDEX_SIZE = vec2(F_MASK_INDEX_SIZE/TEXTURE_WIDTH, F_MASK_INDEX_SIZE/TEXTURE_HEIGHT);
#elif defined (PIXEL_SHADER)
	const float NOISE_REPEAT_SIZE = 1.0/29.0;

	const float NOISE_X_OFFSET = 16.0;
	const float NOISE_Y_OFFSET = 16.0;
	const float F_NOISE_SIZE = 512.0;

	const vec2 NOISE_START = vec2(NOISE_X_OFFSET/TEXTURE_WIDTH, NOISE_Y_OFFSET/TEXTURE_HEIGHT);
	const vec2 NOISE_SIZE = vec2(F_NOISE_SIZE/TEXTURE_WIDTH, F_NOISE_SIZE/TEXTURE_HEIGHT);
#endif
// Lighting
varying vec3 PS_POS;

// xy = min, zw = max
uniform vec4 LIGHTMAP_WORLD_EXTENTS;

#define LIGHTMAP_TEXTURE SAMPLER[3]

#ifndef LIGHTMAP_TEXTURE
	#error If you use lighting, you must #define the sampler that the lightmap belongs to
#endif

vec3 CalculateLightingContribution()
{
	vec2 uv = ( PS_POS.xz - LIGHTMAP_WORLD_EXTENTS.xy ) * LIGHTMAP_WORLD_EXTENTS.zw;

	return texture2D( LIGHTMAP_TEXTURE, uv.xy ).rgb;
}

vec3 CalculateLightingContribution( vec3 normal )
{
	return vec3( 1, 1, 1 );
}
vec4 get_noise()
{
	vec2 TEXCOORD_NOISE = fract(PS_POS.xz * NOISE_REPEAT_SIZE);
	vec4 noise = texture2D(SAMPLER[0], (NOISE_START + NOISE_SIZE * TEXCOORD_NOISE), -100.0);
		
	return noise;
}
void main()
{
	vec4 colour = texture2D(SAMPLER[0], PS_TEXCOORD);

	if (colour.a > 0.00)
	{
		vec4 noise = get_noise();

		colour.rgb *= noise.rgb * colour.a;
		colour = vec4( colour.rgb * PS_COLOUR.rgb, colour.a * PS_COLOUR.a );
		colour.rgb *= CalculateLightingContribution();

		gl_FragColor = vec4(colour.rgb, 0.9 * colour.a);
	}
	else
	{
		discard;
	}
}
                 