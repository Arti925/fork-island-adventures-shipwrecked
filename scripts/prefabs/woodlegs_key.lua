local assets_keys = {
    Asset("ANIM", "anim/woodlegs_key.zip"),
}

local function fn_keys(number)
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBuild("woodlegs_key")
    inst.AnimState:SetBank("woodlegs_key")
    inst.AnimState:PlayAnimation("key"..number)

    MakeInventoryPhysics(inst)

    MakeInventoryFloatable(inst)
    inst.components.floater:UpdateAnimations("key"..number.."_water", "key"..number)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inventoryitem")

    inst:AddComponent("inspectable")

    inst:AddComponent("klaussackkey")

    MakeHauntableLaunch(inst)

    return inst
end

local function MakeKey(number)
    return Prefab("woodlegs_key"..number, function()
        return fn_keys(number)
    end, assets_keys, {})
end

return MakeKey(1), MakeKey(2), MakeKey(3)