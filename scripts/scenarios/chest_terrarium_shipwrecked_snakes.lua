local chestfunctions = require("scenarios/chestfunctions")
local terrarium_loot = require("scenarios/chest_terrarium_shipwrecked")

local function triggertrap(inst, scenariorunner, data)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, 6, {"vine"}, {"INLIMBO"})
	for _, ent in ipairs(ents) do
		if ent.prefab == "bush_vine" then
			local snakeden = SpawnPrefab("snakeden")
			if snakeden then 
				snakeden.Physics:Teleport(ent.Transform:GetWorldPosition())
				ent:Remove()
			end
		end
	end
end

local function OnLoad(inst, scenariorunner)
    chestfunctions.InitializeChestTrap(inst, scenariorunner, triggertrap, 1.0)
end

return
{
    OnLoad = OnLoad,
	OnCreate = terrarium_loot.OnCreate,
    OnDestroy = chestfunctions.OnDestroy,
}
