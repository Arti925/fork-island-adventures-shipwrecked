-- This speech is for Wanda
return {

	ACTIONFAIL =
	{
		REPAIRBOAT = 
		{
			GENERIC = "That'd be a waste.",
		},
		EMBARK = 
		{
			INUSE = "Ohh botheration. I was going to use that, you know!",
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		OPEN_CRAFTING  = 
   		{
        FLOODED = "Those things don't work in water? Botheration, so annoying!",
		}, 

	},
	
	ANNOUNCE_MAGIC_FAIL = "Hmm... Maybe it needs to unwind?",
	
	ANNOUNCE_SHARX = "Ack! Leave me alone!",
	
	ANNOUNCE_TREASURE = "It's about time we found something good!",
	ANNOUNCE_TREASURE_DISCOVER = "Sounds like the clock has struck treasure.",
	ANNOUNCE_MORETREASURE = "So much uncovered at once! How delightfully efficient!",
	ANNOUNCE_OTHER_WORLD_TREASURE = "Awfully far to go for treasure. Maybe another time.",
	ANNOUNCE_OTHER_WORLD_PLANT = "Please, that'd be a waste.",
	
	ANNOUNCE_IA_MESSAGEBOTTLE =
	{
		"Ugh, I won't waste time trying to decipher this mess!",
	},
	ANNOUNCE_VOLCANO_ERUPT = "Ohhh botheration, did I forget to feed it again?",
	ANNOUNCE_MAPWRAP_WARN = "Goodness, I feel such spatial nonsense emanating from beyond!",
	ANNOUNCE_MAPWRAP_LOSECONTROL = "N-no, it's pulling me in!",
	ANNOUNCE_MAPWRAP_RETURN = "Whew... That's scary no matter how many times I've had it happen.",
	ANNOUNCE_CRAB_ESCAPE = "Botheration. Go figure, it's gone already.",
	ANNOUNCE_TRAWL_FULL = "Time to take a look at my haul!",
	ANNOUNCE_BOAT_DAMAGED = "My boat's damaged...",
	ANNOUNCE_BOAT_SINKING = "Ack! This doesn't seem seaworthy at all anymore!",
	ANNOUNCE_BOAT_SINKING_IMMINENT = "I'm not meeting my end drowning in the middle of nowhere!",
	ANNOUNCE_WAVE_BOOST = "Wh-whoa!",
	
	ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "It's been around here not long ago. I can tell.",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "Ugh, this was a complete waste of time!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "Bother, it's hard to see bubbles in this rain!",
	
	DESCRIBE = {
	
		FLOTSAM = "A sad leftover from a ship of long ago.",
		SUNKEN_BOAT = 
		{
			GENERIC = "Aww, who's the cutest little pirate cuckoo?",
			ABANDONED = "She's long gone from her ship.",
		},
		
		SUNKEN_BOAT_BURNT = "Not even my time travel could fix a mess like that.",
		SUNKBOAT = "This doesn't inspire much confidence...",

		BOAT_LOGRAFT = "I don't have time. If it floats, it's enough.",
		BOAT_RAFT = "Alright, I suppose it was worth taking the time to make a better raft.",
		BOAT_ROW = "Rowing takes forever! Surely I can install something on it?",
		BOAT_CARGO = "I'll save so much time if I don't have to go back to fetch my things!",
		BOAT_ARMOURED = "I'm all in for anything that'll keep me safe.",
		BOAT_ENCRUSTED = "Well it's definitely sturdy, but why does it wear so fast with use?",
		CAPTAINHAT = "I'm not sure if this suits me...",

		BOAT_TORCH = "This will keep me safe, nice and hands-free.",
		BOAT_LANTERN = "I should keep it close at sea, just in case.",
		BOATREPAIRKIT = "Repairing takes a lot shorter than building a whole new boat from the scratch.",
		BOATCANNON = "This seems dangerous... But I'll use it if I need to!",

		BOTTLELANTERN = "Ah. That feels much safer to have.",
		BIOLUMINESCENCE = "I'll take a bit of anything that can make its own light.",

		BALLPHIN = "Botheration, they're so distractingly adorable!",
		BALLPHINHOUSE = "I won't even question how or in how long they built that with their noses...",
		DORSALFIN = "That's a bit upsetting.",
		TUNACAN = "Hmm, this lunch was packed with its own temporally-isolated pocket.",

		JELLYFISH = "Better not touch them, we don't want a repeat of last time...",
		JELLYFISH_DEAD = "I wonder if it is really worth the bother.",
		JELLYFISH_COOKED = "It's chewy, but becomes bearable as you get used with time.",
		JELLYFISH_PLANTED = "I think I'm supposed to sail away from these...",
		JELLYJERKY = "If only my teeth were younger... Though I suppose I could do something about that.",
		RAINBOWJELLYFISH = "Oh, these aren't the ones that shock you. Whew...",
		RAINBOWJELLYFISH_PLANTED = "I don't have time to sit and watch the rainbows.",
		RAINBOWJELLYFISH_DEAD = "It's still glowing...",
		RAINBOWJELLYFISH_COOKED = "If only they tasted as good as they looked.",
		JELLYOPOP = "This flummery is chewier than any I've eaten.",

		CROCODOG = "Get away from me!",
		POISONCROCODOG = "Leave me alone! You make me sick!",
		WATERCROCODOG = "Bother, it's getting water everywhere!",
	
		PURPLE_GROUPER = "Raw fish is fine as it is, I don't have time.",
		PIERROT_FISH = "I thought yellow and stripes indicated poison... I must've remembered wrong.",
		NEON_QUATTRO = "How strange, it's cold to the touch.",
		PURPLE_GROUPER_COOKED = "Why do I always forget how much better cooked fish tastes...",
		PIERROT_FISH_COOKED = "Aha, now I remember! These are fine to eat.",
		NEON_QUATTRO_COOKED = "Is there really nothing to eat but fish out here?",
		TROPICALBOUILLABAISSE = "Eating well is a necessity for a ripe old age!",

		FISH_FARM = 
		{
			EMPTY = "Oh bother, now I have to spend time scurrying for fish eggs.",
			STOCKED = "Ohhhhh can't you hatch any faster?",
			ONEFISH = "There's just one? After all this time?",
			TWOFISH = "Will they produce any faster if I leave the second one there?",
			REDFISH = "Finally, a third! Maybe we can have dinner now?",
			BLUEFISH  = "How delightful! So much fish could feed me for days!",
		},
	
		ROE = "There's many tiny little lives inside.",
		ROE_COOKED = "It's supposed to be fancy, but it's yet another strange island food.",
		CAVIAR = "Aha! I didn't forget the jar this time around!",

		CORMORANT = "I don't have time to watch the birds.",
		SEAGULL = "Shoo! Shoo, I said!",
		SEAGULL_WATER = "Shoo! Shoo!",
		TOUCAN = "What's it supposed to do with a beak so large?",
		PARROT = "Are those the ones that speak? How delightfully odd!",
		PARROT_PIRATE = "This cuckoo has a cute little hat and an accent!",

		SEA_YARD =
		{
			ON = "Can't it work any faster? Bother, that's so slow.",
			OFF = "Botheration, did I forget the switch off again?",
			LOWFUEL = "It needs more tar if it's to keep working.",
		},
	
		SEA_CHIMINEA = 
		{
			EMBERS = "I need to give it something! Quick!",
			GENERIC = "I don't even know where to begin with how this could go horribly wrong.",
			HIGH = "That'll keep the darkness away for now.",
			LOW = "It needs to burn more if I want it to last.",
			NORMAL = "That feels better.",
			OUT = "I could light it again in no time at all.",
		}, 

		CHIMINEA = "It'll be easier to keep lit when the winds aren't undoing my work.",

		TAR_EXTRACTOR =
		{
			ON = "It's much faster than extracting by ourselves!",
			OFF = "I'm not sure if we put a switch on this one...",
			LOWFUEL = "It needs something to burn soon.",
		},

		TAR = "Goodness, what a horrid smell.",
		TAR_TRAP = "I really wouldn't want to spend one second stuck on that goop.",
		TAR_POOL = "I was wondering what that terrible smell was.",
		TARLAMP = "It's better than darkness, I suppose...",
		TARSUIT = "Must I put it on? Is there truly nothing else?",

		PIRATIHATITATOR =
		{
			GENERIC = "It makes more sense if you don't let it pillage one second of your thoughts.",
			BURNT = "I'll be just fine not spending any time to make another one.",
		},

		PIRATEHAT = "This looks like something you'd see at a thrift shop or a flea market.",

		MUSSEL_FARM =
		{
			GENERIC = "A bunch of mussels. Moving on.",
			STICKPLANTED = "Hurry up and stick to that dumb rod already!"
		},

		MUSSEL = "I've had these with lemon and rice back home.",
		MUSSEL_COOKED = "Not quite a feast, but it'll tide me over.",
		MUSSELBOUILLABAISE = "Oh, that soup smells so good...",
		MUSSEL_BED = "It'll save a bunch of time if we don't need to seafare for dinner each time.",
		MUSSEL_STICK = "It makes more sense the less you think about it.",

		LOBSTER = "I'm sensing a boiling pot of water in your future.",
		LOBSTER_DEAD = "It's already dead. There's no point in wasting it.",
		LOBSTER_DEAD_COOKED = "Time for dinner!",
		LOBSTERHOLE = "Something's peering out from down there...",
		WOBSTERBISQUE = "Would it be a frivolous use of time travel to go back and eat it over and over again?",
        WOBSTERDINNER = "Mmmm, hot buttered wobster!",
		SEATRAP = "It looks silly, but if it catches lobsters any faster...",

		BUSH_VINE =
		{
			BURNING = "It burns quite dramatically.",
			BURNT = "I will be okay if it doesn't heal from that.",
			CHOPPED = "It's been thoroughly chopped.",
			GENERIC = "At least my hair doesn't have snakes in it.",
		},
		VINE = "It's a vine. I won't take the time to get sentimental about it.",
		DUG_BUSH_VINE = "I should plant this somewhere convinient.",
	
		ROCK_LIMPET =
		{
			GENERIC = "Are those things... Edible?",
			PICKED = "I don't have time to gawk at bare rocks!",
		},

		LIMPETS = "I'm starting to have second thoughts about this...",
		LIMPETS_COOKED = "They taste so much better if you close your nose.",
		BISQUE = "It's fancy, but it doesn't take long to cook.",

		MACHETE = "It's for chopping, simple as that.",
		GOLDENMACHETE = "What else do you do with gold in a jungle island?",

		THATCHPACK = "It's a bit small, but I guess it'll do...",
		PIRATEPACK = "How curious! I believe it may have a wormhole embedded inside.",
		SEASACK = "It'll keep my things wet, salty and safe from the ravages of time.",

		SEAWEED_PLANTED =
        {
            GENERIC = "It's seaweed. Moving on.",
            PICKED = "How interesting, harvested seaweed! Anyways.",
        },

		SEAWEED = "I suppose I don't have the time to be picky with food...",
		SEAWEED_COOKED = "It looks even less appealing when it's cooked.",
		SEAWEED_DRIED = "It's crunchy and flaky, not so bad at all.",
		SEAWEED_STALK = "I suppose we could cultivate these somewhere up close.",

		DUBLOON = "I'll keep a handful in my pocket for good fortune.",
		SLOTMACHINE = "Would it be cheating if I turned back time whenever I got something bad?",
		
		SOLOFISH = "What an adorable, delightfully odd critter!",
		SOLOFISH_DEAD = "This makes me a bit upset, but it'll feed me for the time being.",
		SWORDFISH = "Get away from me! I don't have time to entertain you, fish!",
		SWORDFISH_DEAD = "Eating this seems like it'd be a waste.",
		CUTLASS = "It's definitely sharp, if only I could apply shadow magic to it...",

		SUNKEN_BOAT_TRINKET_1 = "I think I used to know how to make use of this...", --sextant
		SUNKEN_BOAT_TRINKET_2 = "I should give this one to the kids.", --toy boat
		SUNKEN_BOAT_TRINKET_3 = "It could serve as decoration, for a while at least.", --candle
		SUNKEN_BOAT_TRINKET_4 = "It's a meticulously-made interdimensional device. How curious!", --sea worther
		SUNKEN_BOAT_TRINKET_5 = "It's got a hole in it. It also reeks.", --boot
		TRINKET_IA_13 = "I think the expiry date of that one has long passed...", --orange soda
		TRINKET_IA_14 = "Be wary, I can feel the shadow magic in this.", --voodoo doll
		TRINKET_IA_15 = "Like I'd have time to sit down and strum some silly song!", --ukulele
		TRINKET_IA_16 = "I don't think this is meant to be in this time period.", --license plate
		TRINKET_IA_17 = "It's got a hole in it. It also reeks.", --boot
		TRINKET_IA_18 = "Perfect. Just what I always wanted, a broken vase!", --vase
		TRINKET_IA_19 = "This reminds me of a prescription I used in the past...", --brain cloud pill
		TRINKET_IA_20 = "I think I used to know how to make use of this...", --sextant
		TRINKET_IA_21 = "I should give this one to the kids.", --toy boat
		TRINKET_IA_22 = "It could serve as decoration, for a while at least.", --wine candle
		TRINKET_IA_23 = "Definitely does not belong to this place or time.", --broken aac device
		EARRING = "It's about as interesting as one sock.",
		
		TURBINE_BLADES = "Maybe this could speed up the dreadfully slow sailing trips.",

		TURF_BEACH = "Why am I wasting time staring at the ground?",
		TURF_JUNGLE = "Why am I wasting time staring at the ground?",
		TURF_MAGMAFIELD = "Why am I wasting time staring at the ground?",
		TURF_TIDALMARSH = "Why am I wasting time staring at the ground?",
		TURF_ASH = "Why am I wasting time staring at the ground?",
		TURF_MEADOW = "Why am I wasting time staring at the ground?",
		TURF_VOLCANO = "Why am I wasting time staring at the ground?",
		TURF_SWAMP = "Why am I wasting time staring at the ground?",
		TURF_SNAKESKIN = "I missed carpet...",

		WHALE_BLUE = "What a sad old thing.",
		WHALE_CARCASS_BLUE = "Its time came soon enough.",
		WHALE_WHITE = "Bother, this one has an attitude problem!",
		WHALE_CARCASS_WHITE = "I didn't think that we'd get along this time around, either.",
		WHALE_TRACK = "Let's hurry up, there's a whale to find.",
		WHALE_BUBBLES = "That seems out of place...",
		BLUBBERSUIT = "I don't have time to fret over indignities.",
		BLUBBER = "Not even worth the time it takes to bear and chew it.",
		HARPOON = "Let's have the frail clockmaker do the hunting, why not.",

		SAIL_PALMLEAF = "It should help my boat pick up the pace.",
		SAIL_CLOTH = "This one's clearly better and faster than the last.",
		SAIL_SNAKESKIN = "There's no need to make a flag if the sail is colorful. Not that I need one.",
		SAIL_FEATHER = "Too bad it's not as fast as flying.",
		IRONWIND = "Finally, something as fast as I need!",


		BERMUDATRIANGLE = "That seems like an outworldly space-time anomaly...",
	
		PACKIM_FISHBONE = "A dead fish, just what I always wanted!",
		PACKIM = "I'd better remember not to entrust him with fish, it always ends up the same.",

		TIGERSHARK = "I hate that it's not even the scariest thing I've seen around here.",
		MYSTERYMEAT = "Ugh. Not even the passage of time will touch that.",
		SHARK_GILLS = "I suppose I could tinker with it until I find an use worth my time.",
		TIGEREYE = "It's still unsettling...",
		DOUBLE_UMBRELLAHAT = "This looks silly, but at least I can't forget that at home.",
		SHARKITTEN = "You'd be a lot cuter were it not for the razor-teeth...",
		SHARKITTENSPAWNER = 
		{
			GENERIC = "I think I just heard a yawn from the inside...",
			INACTIVE = "They took off, for the time being.",
		},

		WOODLEGS_KEY1 = "Something, somewhere must be locked.",--Unused
		WOODLEGS_KEY2 = "This key probably unlocks something.",--Unused
		WOODLEGS_KEY3 = "That's a key.",--Unused
		WOODLEGS_CAGE = "Someone must've been very naughty to be locked up like that.",--Unused

		CORAL = "Their beauty resists the flow of time.",
		ROCK_CORAL = "It's a coral rock.",
		LIMESTONENUGGET = "It may resist time, but not the grinding stone.",
		NUBBIN = "What an alien-looking thing.",
		CORALLARVE = "I hope this was worth all the time and effort.",
		WALL_LIMESTONE = "It feels safe, for now.",
		WALL_LIMESTONE_ITEM = "I should take the time to build it.",
		WALL_ENFORCEDLIMESTONE = "Maybe those horrible crocodiles won't get past these.",
		WALL_ENFORCEDLIMESTONE_ITEM = "I should take the time to build it.",
		ARMORLIMESTONE = "This seems like a quick way to sink myself.",
		CORAL_BRAIN_ROCK = "How curious. Maybe I'll spend a moment to take a closer look...",
		CORAL_BRAIN = "It's gross, but oddly comforting.",
		BRAINJELLYHAT = "This will spare us many trips.",

		SEASHELL = "Wow, amazing. Moving on.",
		SEASHELL_BEACHED = "Ooh a seashell, how unexpected.",
		ARMORSEASHELL = "This seems a tad frivolous... But it will protect me.",

		ARMOR_LIFEJACKET = "I'd rather not meet an untimely end in the middle of the sea.",
		ARMOR_WINDBREAKER = "Pink's not the color I'd pick at my age...",

		SNAKE = "Get lost! You won't bite me!",
		SNAKE_POISON = "Stay! Stay away, nasty thing!",
		SNAKESKIN = "Whew... It's not attached to a snake this time around.",
		SNAKEOIL = "So many timelines and I've still not figured out what this is for.",
		SNAKESKINHAT = "If it keeps me from dying to wetness-induced pneumonia, I'll be happy.",
		ARMOR_SNAKESKIN = "I suppose this is a good use for those nasty snakes.",
		SNAKEDEN =
		{
			BURNING = "A quick way to get rid of those pesky snakes.",
			BURNT = "That didn't take long. Good.",
			CHOPPED = "It's been thoroughly chopped.",
			GENERIC = "I think I heard hissing from that one...",
		},

		OBSIDIANFIREPIT =
		{
			EMBERS = "I-I need something to burn, quick!",
			GENERIC = "That's a comfort.",
			HIGH = "A fire this size should last a good while!",
			LOW = "It needs to burn more.",
			NORMAL = "An efficient fire pit will take shorter to light.",
			OUT = "I could get it again going in no time.",
		},

		OBSIDIAN = "If I had sharp enough cutting tools, this could go well as a watch crystal.",
		ROCK_OBSIDIAN = "Trying to mine it would be a waste of time and effort.",
		OBSIDIAN_WORKBENCH = "It reminds me of my own workshop, just obviously hotter.",
		OBSIDIANAXE = "I'm not sure if it offsets the time spent fretting that you don't start a fire.",
		OBSIDIANMACHETE = "It can double as a hand-warmer after just a little time working.",
		SPEAR_OBSIDIAN = "I can appricerate a sharp weapon that makes its own light.",
		VOLCANOSTAFF = "There are just so many ways this could go horribly wrong...",
		ARMOROBSIDIAN = "I'd rather not be singed if I can help it.",
		COCONADE =
		{
			BURNING = "It's about to blow, quick!",
			GENERIC = "This seems like a bad idea...",
		},

		OBSIDIANCOCONADE =
		{
			BURNING = "Quick, before it blows up!",
			GENERIC = "This has danger written all over it.",
		},

		VOLCANO_ALTAR =
		{
			GENERIC = "Thankfully, it seems calm for the time being.",
			OPEN = "Did I forget to feed it? Oh well, just in case.",
		},

		VOLCANO = "Let's just climb a mountain that can erupt any moment, why not.",
		VOLCANO_EXIT = "That's enough of this place!",
		ROCK_CHARCOAL = "I should snatch it up, it'd take shorter than burning trees.",
		VOLCANO_SHRUB = "An old sad tree.",
		LAVAPOOL = "It seems like a quick way to burn myself.",
		COFFEEBUSH =
		{
			BARREN = "Are these the ones that need ash or manure? I can't remember...",
			WITHERED = "It needs something burnt. What a bother.",
			GENERIC = "Aha! Coffee!",
			PICKED = "If only I could figure out how to grow it faster.",
		},

		COFFEEBEANS = "A quick roasting will get me something good to sip on.",
		COFFEEBEANS_COOKED = "I could pop these in my mouth or take the time to boil them.",
		DUG_COFFEEBUSH = "It'll save so much time if we have the coffee wherever we want it to be!",
		COFFEE = "Perfect for those long nights at the workshop back home.",

		ELEPHANTCACTUS =
		{
			BARREN = "A horrible cactus.",
			WITHERED = "I should feed it so it can threaten my life, why not.",
			GENERIC = "Ooh you hateful thing!",
			PICKED = "That's what it gets.",
		},

		DUG_ELEPHANTCACTUS = "I could plant this so it can try to kill me wherever I want it to.",
		ELEPHANTCACTUS_ACTIVE = "Ooh you nasty thing!",
		ELEPHANTCACTUS_STUMP = "At least it won't be shooting at me anytime soon.",
		NEEDLESPEAR = "This seems dangerous.",
		ARMORCACTUS = "If it's going to keep me from being impaled, I'll happily wear it.",
		
		TWISTER = "Ohhhh. Right. You're back already.",
		TWISTER_SEAL = "I don't know if it's the same seal, or multiple mischevious ones...",
		MAGIC_SEAL = "I suppose this could have some magical application, with some work.",
		WIND_CONCH = "Manipulating winds can't be much more difficult than manipulating time.",
		WINDSTAFF = "I can go much faster with the wind at my back!",

		DRAGOON = "Ooh you rude thing, stay back!",
		DRAGOONHEART = "I don't like that it's still hot and beating...",
		DRAGOONSPIT = "I'll be sure not to step on that.",
		DRAGOONEGG = "Quick, we should rid of it before it hatches!",
		DRAGOONDEN = "Ugh. Those youth have zero respect for their elders.",

		ICEMAKER = 
		{
			OUT = "It'll make me ice again when I want it to.",
			VERYLOW = "It needs something to burn into ice VERY soon.",
			LOW = "It could use a bit more to burn if I want more ice.",
			NORMAL = "Delightfully quicker than mining glaciers every year.",
			HIGH = "Such piles of ice should do us good for a long time!",
		},

		HAIL_ICE = "Ugh, I don't want these falling on my head!",
	
		BAMBOOTREE =
		{
			BURNING = "Maybe that was meant to be.",
			BURNT = "Nothing left to do but to burn it again later.",
			CHOPPED = "Delightfully, I hear these grow back very fast.",
			GENERIC = "I won't waste my time watching the bamboo grow.",
		},

		BAMBOO = "I could think of a few uses for these later.",
		FABRIC = "I missed fabric...",
		DUG_BAMBOOTREE = "I could put it somewhere more convinient for later.",
		
		JUNGLETREE =
		{
			BURNING = "I suppose that was gonna happen sooner or later.",
			BURNT = "It burned once and it can burn again.",
			CHOPPED = "That took a bit of time and handiwork.",
			GENERIC = "It's a jungle tree. Anyways...",
		},

		JUNGLETREESEED = "There's a little life inside.",
		JUNGLETREESEED_SAPLING = "It will be a new jungle tree in no time at all.",
		LIVINGJUNGLETREE = "Oh, what a horrible face it has!",


		OX = "Not exactly majestic, but somehow even smellier.",
		BABYOX = "So little.",--unused
		OX_HORN = "I should pocket it for some high-degree tinkering later.",
		OXHAT = "That works, but I could think of better uses for those horns...",
		OX_FLUTE = "It's much faster than waiting for it to rain for sure.",

		MOSQUITO_POISON = "Ooh, you horrible thing!",
		MOSQUITOSACK_YELLOW = "I hope this doesn't pop while I have it on me...",

		STUNGRAY = "Keep your stench away from me!",
		POISONHOLE = "I'd better watch my step around here.",
		GASHAT = "I'd do well to take care of my health, at my age.",

		ANTIVENOM = "It's simply a must-have around here.",
		VENOMGLAND = "I suppose I could ingest it as it is, it'd be better than poison...",
		POISONBALM = "What a quick alternative to the classic antidote medicine!",
		
		SPEAR_POISON = "I suppose I'm not above using this, either.",
		BLOWDART_POISON = "It'd induce a slow and painful death, how terrible.",

		SHARX = "I don't care if you're rare, go away or I'll rid of you!",
		SHARK_FIN = "What am I supposed to do with a severed fin?",
		SHARKFINSOUP = "I'm probably supposed to feel bad about this snack.",
		SHARK_TEETHHAT = "I'm not sure how I feel wearing teeth on my head... But it does look fancy.",
		AERODYNAMICHAT = "Well it's quick for sure, but why does it need so much sewing?",

		IA_MESSAGEBOTTLE = "Not the quickest way to communicate, is it?",
		IA_MESSAGEBOTTLEEMPTY = "I could put something in this bottle later.",
		BURIEDTREASURE = "Quick, I want to see what's inside!",

		SAND = "Sand. You do tend to find a lot of it around here.",
		SANDDUNE = "Ooh a pile of sand, how surprising!",
		SANDBAGSMALL = "I look forward to not being up to my knees in floodwater.",
		SANDBAGSMALL_ITEM = "I should line these up, they'll be worth the time to build.",
		SANDCASTLE =
		{
			SAND = "The castle fell to the ravages of time.",
			GENERIC = "It won't even last as long as it took to build..."
		},

		SUPERTELESCOPE = "It'd save so much time over going all the way over there and taking a look myself!",
		TELESCOPE = "This could make exploring much faster.",
		
		DOYDOY = "Look at it waste its life away trying to chew everything.",
		DOYDOYBABY = "So young, so full of vigor.",
		DOYDOYEGG = "It's a bird, or a breakfast.",
		DOYDOYEGG_COOKED = "Tastes just like stolen time.",
		DOYDOYFEATHER = "Ooh, such an enormous feather!",
		DOYDOYNEST = "The stench is indescribable.",
		TROPICALFAN = "A quick solution to not die to heat exposure.",
	
		PALMTREE =
		{
			BURNING = "That's alarming...",
			BURNT = "That didn't last long, but it can burn again.",
			CHOPPED = "Its time was cut short.",
			GENERIC = "Maybe I could rest my back under it for a moment or two.",
		},

		COCONUT = "I need something to hack it. What a tedious fruit.",
		COCONUT_HALVED = "I wonder if there's enough flesh inside to be worth it at all.",
		COCONUT_COOKED = "At least it seems easier to eat now.",
		COCONUT_SAPLING = "It will be a palm tree in no time at all.",
		PALMLEAF = "It looks like something you'd see at a needlessly expensive hotel resort.",
		PALMLEAF_UMBRELLA = "I suppose it's a lot less effort than fanning myself with it.",
		PALMLEAF_HUT = "Maybe I'll take a moment under it to cool down...",
		LEIF_PALM = "Mind your own business, you horrible hateful thing!",

		CRAB = 
		{
			GENERIC = "A crabbit... Or is it?",
			HIDDEN = "Botheration, I can't reach it down there!",
		},

		CRABHOLE = "I'd rather not fall into a crab hole.",

		TRAWLNETDROPPED = 
		{
			SOON = "It will sink in nearly no time at all.",
			SOONISH = "I'd best empty it before it sinks by itself.",
			GENERIC = "Now to check if that was worth the time.",
		},

		TRAWLNET = "At least it's faster than fishing for treasure myself.",
		IA_TRIDENT = "A weapon from who knows how long ago.",

		KRAKEN = "Oh botheration, not this one again!",
		KRAKENCHEST = "I hope the spoils are worth having dealt with that mess.",
		KRAKEN_TENTACLE = "Keep away from my boat!",
		QUACKENBEAK = "It'll take a while until I figure out something to use this for.",
		QUACKENDRILL = "It'll be faster to secure tar if the slick is up close.",
		QUACKERINGRAM = "This seems dangerous, but I'm not afraid to use it!",

		MAGMAROCK = "It's a pile of rocks. Moving on.",
		MAGMAROCK_GOLD = "That could be worth digging, I can see something shiny in there.",
		FLAMEGEYSER = "Ooh, how magnificent!",

		TELEPORTATO_SW_RING = "Looks like I could use this.",--unused
		TELEPORTATO_SW_BOX = "It looks like a part for something.",--unused
		TELEPORTATO_SW_CRANK = "I wonder what this is used for.",--unused
		TELEPORTATO_SW_BASE = "I think it's missing some parts.",--unused
		TELEPORTATO_SW_POTATO = "Seems like it was made with a purpose in mind.",--unused

		PRIMEAPE = "They're up to their old tricks.",
		PRIMEAPEBARREL = "I'm not sure I want to take one second digging into that.",
		MONKEYBALL = "Imagine getting so distracted by a ball of cloth.",
		WILBUR_UNLOCK = "He look like king!",--unused
		WILBUR_CROWN = "It for monkey!",--unused

		MERMFISHER = "No point in wasting the fish if they're not eating.",
		MERMHOUSE_FISHER = "All this time and I haven't figured out why they impale fish all day.",

		OCTOPUSKING = "Look at him, spending all his time cuddled up on a rock.",
		OCTOPUSCHEST = "At least he can be generous at times.",

		SWEET_POTATO = "It's sweet and it's going right into my mouth.",
		SWEET_POTATO_COOKED = "Alright, I suppose it's much easier to eat like this.",
		SWEET_POTATO_PLANTED = "I should snatch that for later.",
		SWEET_POTATO_SEEDS = "Why bother growing them when I can eat them now?",
		SWEETPOTATOSOUFFLE = "Alright, it does smell pretty good.",

		BOAT_WOODLEGS = "Bother, can it not go any faster?!",
		WOODLEGSHAT = "This belongs on someone else's head.",
		SAIL_WOODLEGS = "At least we know who it belongs to.",

		PEG_LEG = "Thankfully none of us need it.",
		PIRATEGHOST = "Stay back! I won't drown like you!",

		WILDBORE = "And they call me impatient!",
		WILDBOREHEAD = "That's a smell I'll never forget.",
		WILDBOREHOUSE = "I'm surprised it doesn't blow down in a storm.",

		MANGROVETREE = "A tree. Let's go on.",
		MANGROVETREE_BURNT = "Well, that's a surprise.",

		PORTAL_SHIPWRECKED = "It's broken.",
		SHIPWRECKED_ENTRANCE = "Ooh, a fancy stationary device for interdimensional travel!",
		SHIPWRECKED_EXIT = "Is it time to go back already?",

		TIDALPOOL = "For some reason, these don't seem to ever dry up.",
		FISHINHOLE = "Oh. Fish. Splendid.",
		FISH_TROPICAL = "It's colorful, I hope that doesn't mean it's poison...",
		TIDAL_PLANT = "I'm not stopping to gawk at every plant I see!",
		MARSH_PLANT_TROPICAL = "Some plant or other.",

		FLUP = "Horrible swamp thing!",
		BLOWDART_FLUP = "This seems frivolous.",

		SEA_LAB = "How is this any more useful than the landborne one?",
		BUOY = "I suppose some light at sea wouldn't bite.", 
		WATERCHEST = "I hope it's truly waterproof, I don't want my pocket watches catching rust.",

		LUGGAGECHEST = "I don't want to think about whoever that belonged to.",
		WATERYGRAVE = "You'll never see me drowned like any of these.",
		SHIPWRECK = "That doesn't inspire much confidence at sea...",
		BARREL_GUNPOWDER = "That sounds like a quick way to catch my death at sea.",
		RAWLING = "Just wait until I expose who you really are, ball!",
		GRASS_WATER = "I don't think that'd make good kindling.",
		KNIGHTBOAT = "Ugh, I've nothing to answer to you!",

		DEPLETED_BAMBOOTREE = "Will it grow again?",--unused?
		DEPLETED_BUSH_VINE = "Will it come back?",--unused?
		DEPLETED_GRASS_WATER = "Poor plant, florp.",--unused?

		WALLYINTRO_DEBRIS = "Shipwrecked.", --unused
		BOOK_METEOR = "I'm not sure if this is her wisest idea...",
		CRATE = "I could quickly smash that open if I had something to do so with.",
		SPEAR_LAUNCHER = "I like that I can shoot with it from far away.",
		MUTATOR_TROPICAL_SPIDER_WARRIOR = "I don't think I will complain about my own cooking anytime soon.",

		CHESSPIECE_KRAKEN = "Another ruffian bites the dust.",
		--CHESSPIECE_TIGERSHARK = "TEMP, put something here",
		--CHESSPIECE_TWISTER = "TEMP, put something here",
		--CHESSPIECE_SEAL = "TEMP, put something here",

		--SWC
		BOAT_SURFBOARD = "I just don't have the time to learn such a pleasantry.",
		SURFBOARD_ITEM = "I just don't have the time to learn such a pleasantry.",

		WALANI = {
            GENERIC = "Sorry %s, no time to rest! Or chill, or swim, or- you get it!",
            ATTACKER = "Strange. Could've swore you were a pacifist in this timeline.",
            MURDERER = "I know what you did this time, %s. Leave me out of it!",
            REVIVER = "If only I could relax with such ease and no regard for the time like you, %s.",
            GHOST = "I'm positive you don't mind waiting a moment!",
            FIRESTARTER = "My timelines must be getting jumbled.",
		},

		WILBUR = {
            GENERIC = "You don't have rabies in this timeline, do you %s?",
            ATTACKER = "I knew %s would start biting eventually.",
            MURDERER = "Stay back! I've no time to train killer orangutans or whatever you are!",
            REVIVER = "Between you and me, my death didn't happen. Okay, %s?",
            GHOST = "Hold on for a moment, I'll get to it when I get to it!",
            FIRESTARTER = "This must be your kingly side showing.",
		},

		WOODLEGS = {
            GENERIC = "Sorry, %s. No time to join another band of misfits.",
            ATTACKER = "Acts like that from you don't surprise me anymore.",
            MURDERER = "Murderer! I'm not sticking around to see where this goes!",
            REVIVER = "You sure are reliable to your...crew? Is it?",
            GHOST = "I know you hate giving things away, but give me a moment, %s!",
            FIRESTARTER = "Keep the cannonballs to your pirate battles, got it?",
		},
	},
}
