local _G = GLOBAL

AddPrefabPostInit("boat", function(inst)
    if _G.TheNet:IsDedicated() and _G.TheWorld:HasTag("island") then
        inst:AddComponent("mapwrapper")
    end
end)