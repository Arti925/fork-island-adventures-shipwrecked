local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

IAENV.AddComponentPostInit("klaussackspawner", function(self)
    if not TheWorld:HasTag("island") then
        return
    end

    local KLAUSSACK_TIMERNAME = "klaussack_spawntimer"
    local OnIsWinterEvent = UpvalueHacker.GetUpvalue(self.OnPostInit, "OnIsWinterEvent")
    local OnIsWinter = UpvalueHacker.GetUpvalue(self.OnPostInit, "OnIsWinter")
    local _spawnedthiswinter = UpvalueHacker.GetUpvalue(self.OnPostInit, "_spawnedthiswinter")
    local _spawnsthiswinter = UpvalueHacker.GetUpvalue(self.OnPostInit, "_spawnsthiswinter")
    local _sack = UpvalueHacker.GetUpvalue(self.OnPostInit, "_sack")
    local OnRespawnTimer = UpvalueHacker.GetUpvalue(self.OnPostInit, "OnRespawnTimer")
    local StartKlausSpawnTimer = UpvalueHacker.GetUpvalue(self.OnPostInit, "StartKlausSpawnTimer")

    function self:OnPostInit()
        local _worldsettingstimer = TheWorld.components.worldsettingstimer
        _worldsettingstimer:StopTimer(KLAUSSACK_TIMERNAME)
        local multiplier
        if not IsSpecialEventActive(SPECIAL_EVENTS.WINTERS_FEAST) then
            _worldsettingstimer:AddTimer(KLAUSSACK_TIMERNAME, TUNING.KLAUSSACK_RESPAWN_DELAY + TUNING.KLAUSSACK_SPAWN_DELAY + TUNING.KLAUSSACK_SPAWN_DELAY_VARIANCE, TUNING.SPAWN_KLAUS, OnRespawnTimer)
            if TheWorld.state.issummer then
                if _spawnedthiswinter then
                    _spawnsthiswinter = 1
                end
                --start a new timer if the conditions have changed after a reload
                if _spawnsthiswinter >= 1 and _spawnsthiswinter < TUNING.KLAUSSACK_MAX_SPAWNS and not _worldsettingstimer:ActiveTimerExists(KLAUSSACK_TIMERNAME) and (_sack == nil or not _sack:IsValid()) then
                    StartKlausSpawnTimer(TUNING.KLAUSSACK_RESPAWN_DELAY)
                end
            end
            self:StopWatchingWorldState("issummer", OnIsWinter)
            self:WatchWorldState("issummer", OnIsWinter)
            OnIsWinter(self, TheWorld.state.issummer)
        else
            self:StopWatchingWorldState("issummer", OnIsWinterEvent)
            self:WatchWorldState("issummer", OnIsWinterEvent)
            OnIsWinterEvent(self, TheWorld.state.issummer)
            _worldsettingstimer:AddTimer(KLAUSSACK_TIMERNAME, TUNING.KLAUSSACK_EVENT_RESPAWN_TIME, TUNING.SPAWN_KLAUS, OnRespawnTimer)
            if _sack == nil and not _worldsettingstimer:ActiveTimerExists(KLAUSSACK_TIMERNAME) then
                OnRespawnTimer() -- spawns on day 1 for winters feast event
                if TheWorld.state.issummer then
                    _spawnsthiswinter = _spawnsthiswinter + 1
                end
            end
        end
    end

    self:OnPostInit()
end)